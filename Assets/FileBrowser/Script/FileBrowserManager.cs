using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class FileBrowserManager : Singleton<FileBrowserManager> {
	//skins and textures
	[SerializeField]
	private GUISkin[] skins;
	[SerializeField]
	private GUIStyle textGUIStyle;
	[SerializeField]
	private Texture2D[] fileTextures;
	[SerializeField]
	private Texture2D file,folder,back,drive;
	[SerializeField]
	private string defaultDirectory = "";
	[SerializeField]
	private GameObject filePanel;
	private GameObject imageContainer;
	private GameObject videoContainer;
	[SerializeField]
	private string[] allowedFileExtensions;
	
	private string[] layoutTypes = {"Type 0","Type 1"};
	private FileBrowser fb = new FileBrowser(); //initialize file browser
	private string output = "No file selected";
	private BrainFile selectedFile;
	private string activeFileName;

	private Dictionary<string, BrainFile> brainFiles;
	public Dictionary<string, BrainFile> BrainFiles { get{return brainFiles;} }

	public Texture2D[] FileTextures { get {return fileTextures;} }
	public Texture2D DefaultFileTexture { get {return file;} }

	public GameObject FilePanel { get { return filePanel;} }

	protected override void Awake()
	{
		base.Awake ();
		imageContainer = filePanel.transform.FindChild ("ImageContainer").gameObject;
		videoContainer = filePanel.transform.FindChild ("VideoContainer").gameObject;
	}

	// Use this for initialization
	private void Start () {
		brainFiles = new Dictionary<string, BrainFile> ();
		//setup file browser style
		fb.guiSkin = skins[0]; //set the starting skin
		//set the various textures
		fb.fileTexture = file; 
		fb.directoryTexture = folder;
		fb.backTexture = back;
		fb.driveTexture = drive;
		//show the search bar
		fb.showSearch = false;
		//search recursively (setting recursive search may cause a long delay)
		fb.searchRecursively = false;

		fb.setGUIRect (new Rect(0f,0f,Screen.width*0.25f,Screen.height*1f));
		fb.setLayout(1);
		fb.setDirectory (Directory.GetCurrentDirectory () + defaultDirectory);
		fb.setAllowedFileExtensions (allowedFileExtensions);
		fb.setTextStyle (textGUIStyle);
		fb.InitializeSubfoldersFiles ();
	}

	private void OnGUI(){
		//DisplaySelectionMenu ();
		if(fb.draw()) //true is returned when a file has been selected
		{ 
			//the output file is a member of the FileInfo class, if cancel was selected the value is null
			if(fb.outputFile != null)
			{
				selectedFile = fb.outputFile.bf;

				if(selectedFile.fileType == BrainFile.BrainFileType.Image)
				{
					ImageFile img = (ImageFile)selectedFile;
					img.SetIsNew (false);
					fb.outputFile.SetGUIContent (); //Update file texture from New to Old
					Sprite sprite = Sprite.Create (img.Image, new Rect(0,0, img.Image.width, img.Image.height), new Vector2 (0.5f, 0.5f));
					imageContainer.GetComponent<Image> ().sprite = sprite;
					imageContainer.SetActive (true);
					videoContainer.SetActive (false);
					imageContainer.GetComponent<SelectionBox> ().ResetSelectionBox ();
					SetActiveFileName (selectedFile.fileInfo.Name);

					//set a reference to the current active game file in File Container.
					imageContainer.GetComponent<FileContainer> ().SetActiveBrainFile(selectedFile);
				}
				else if (selectedFile.fileType == BrainFile.BrainFileType.Audio) 
				{
					AudioFile audio = (AudioFile)selectedFile;
					audio.SetIsNew (false);
					fb.outputFile.SetGUIContent (); //Update file texture from New to Old
					Sprite sprite = Sprite.Create(audio.Image, new Rect(0,0, audio.Image.width, audio.Image.height), new Vector2 (0.5f, 0.5f));
					imageContainer.GetComponent<Image> ().sprite = sprite;
					imageContainer.GetComponent<AudioSource> ().clip = audio.Clip;
					imageContainer.SetActive (true);
					videoContainer.SetActive (false);
					imageContainer.GetComponent<SelectionBox> ().ResetSelectionBox ();
					SetActiveFileName (selectedFile.fileInfo.Name);
					imageContainer.GetComponent<FileContainer> ().SetActiveBrainFile(selectedFile);
				}
				else if (selectedFile.fileType == BrainFile.BrainFileType.Video)
				{
					VideoFile videoFile = (VideoFile)selectedFile;
					videoFile.SetIsNew (false);
					fb.outputFile.SetGUIContent (); //Update file texture from New to Old
					videoContainer.transform.FindChild ("Video").GetComponent<MovieDev> ().SetMovieTexture (videoFile.Video);
					videoContainer.gameObject.SetActive (true);
					imageContainer.SetActive (false);
					SetActiveFileName (selectedFile.fileInfo.Name);
				}
			}
			else
			{
				imageContainer.SetActive (false);
			}
			if(fb.selectedDirectory != null)
			{
				//Select correspondent cortex
				BrainManager.Instance.SelectCortex(fb.selectedDirectory.di.Name);
			}
		}
	}

	private void DisplaySelectionMenu()
	{
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();

		//Layout type options
		GUILayout.Label("Layout Type");
		fb.setLayout(GUILayout.SelectionGrid(fb.layoutType,layoutTypes,1));
		fb.setLayout(GUILayout.SelectionGrid(fb.layoutType,layoutTypes,1));

		GUILayout.Space(10);

		//GUISkin options
		GUILayout.Label("GUISkin");
		foreach(GUISkin s in skins){
			if(GUILayout.Button(s.name)){
				fb.guiSkin = s;
			}
		}

		//Search bar options
		GUILayout.Space(10);
		fb.showSearch = GUILayout.Toggle(fb.showSearch,"Show Search Bar");
		fb.searchRecursively = GUILayout.Toggle(fb.searchRecursively,"Search Sub Folders");
		GUILayout.EndVertical();
		
		//Selected file string
		GUILayout.Space(10);
		GUILayout.Label("Selected File: "+output);
		GUILayout.EndHorizontal();
	}

	public string GetActiveFileName()
	{
		return activeFileName;
	}

	private void SetActiveFileName(string fileName)
	{
		activeFileName = fileName;
	}

	public void OpenDirectory(string directoryName)
	{
		string path = fb.GetDefaultDirectory () + "/" + directoryName;
		fb.getFileList (new DirectoryInfo (path));
	}

	public bool IsFilePanelActive()
	{
		foreach (Transform child in filePanel.transform)
		{
			if(child.gameObject.activeSelf)
			{
				return true;
			}
		}
		return false;
	}

}
