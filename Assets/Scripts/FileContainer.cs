﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// This class is responsible for using the Debug.Drawline tool to draw a waveform by accessing the audio sample array every set increment.
/// </summary>
public class FileContainer : MonoBehaviour
{
	[SerializeField]
	Texture playLine;

	float[] samples;
	AudioSource aud;
	float clipLength;
	float secondsPerPixel;

	BrainFile activeBrainFile;
	RectTransform myRectTransform;

	private GameObject controllers;
	/*
	//The interval at which the sample array is accessed
	[SerializeField]
	int sampleIncrement;

	//The amount by which the x-axis is incremented each time the samples are accessed
	[SerializeField]
	float lineWidth;

	// height of the wave form
	[SerializeField]
	float yAxisModifier;

	// duration that the waveform stays on screen
	[SerializeField]
	int lineDuration;*/

	Vector2 debugStart, debugEnd;

//	[SerializeField]
//	Sprite waveform;

	Image myImage;


	// Use this for initialization
	void Awake () 
	{
		myImage = GetComponent<Image> ();
		controllers = transform.FindChild ("Controllers").gameObject;
		myRectTransform = this.GetComponent<RectTransform> ();
	}

	void LoadAudio()
	{
		aud = GetComponent<AudioSource>();
		clipLength = aud.clip.length;

		CalculateSecondsPerPixel ();
	}
	
	void OnGUI( )
	{
		if (activeBrainFile.fileType == BrainFile.BrainFileType.Audio) 
		{
			if (aud.isPlaying) 
			{
				float rectX, rectY, height;
				rectX = (Screen.width - myRectTransform.rect.width) + (aud.time / secondsPerPixel);
				rectY = 1;
				height = myRectTransform.rect.height;
				Rect drawRect = new Rect (rectX, rectY, playLine.width, height);
				GUI.DrawTexture(drawRect,playLine);
			}
		}
	}

	/// <summary>
	/// Calculates the seconds per pixel value for the given waveform image.
	/// </summary>
	public void CalculateSecondsPerPixel()
	{
		RectTransform imgRectTransform = GetComponent<RectTransform> ();

		if (imgRectTransform) 
		{
			secondsPerPixel = clipLength / imgRectTransform.rect.width;
		}
	}

	/// <summary>
	/// Playes audio according to selection
	/// </summary>
	public void PlayAudio()
	{
		Rect selectionBox = GetComponent<SelectionBox> ().GetSelectionRectangleInScreen ();

		//If there is a selection box, play that section only
		if (selectionBox.width != 0) 
		{
			float xPos;
			RectTransform imgRectTransform = GetComponent<RectTransform> ();

			if (selectionBox.x < (selectionBox.x + selectionBox.width)) 
			{
				xPos = selectionBox.x - (Screen.width - imgRectTransform.rect.width);
			}
			else 
			{
				xPos = (selectionBox.x + selectionBox.width) - (Screen.width - imgRectTransform.rect.width);
			}

			float startTime = xPos * secondsPerPixel;
			float duration = Mathf.Abs(selectionBox.width) * secondsPerPixel;

			aud.time = xPos * secondsPerPixel;
			aud.PlayScheduled (startTime);
			aud.SetScheduledEndTime (AudioSettings.dspTime + duration);
		}

		//Otherwise play the whole clip
		else 
		{
			aud.time = 0;
			aud.Play ();
		}
	}

	/// <summary>
	/// Stops audio playback if audioclip is playing.
	/// </summary>
	public void StopAudio()
	{
		if (aud.isPlaying) 
		{
			aud.Stop ();
		}
	}


	/// <summary>
	/// Disables file container and shows the brain behind
	/// </summary>
	public void ShowBrain()
	{
		this.gameObject.SetActive (false);
	}

	public void SetActiveBrainFile(BrainFile file)
	{
		activeBrainFile = file;

		if(activeBrainFile.fileType == BrainFile.BrainFileType.Audio)
		{
			LoadAudio ();
			controllers.SetActive (true);
		}
		else if(activeBrainFile.fileType == BrainFile.BrainFileType.Image)
		{
			controllers.SetActive (false);
		}

		//Debug.Log (activeBrainFile.fileInfo.Name + " " + activeBrainFile.fileType + " Loaded");
	}

	public BrainFile GetActiveBrainFile()
	{
		return activeBrainFile;
	}

	/*
	/// <summary>
	/// Raises the pointer down event. Prints the mouse location in pixels to the Debug Log
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerDown(PointerEventData eventData) // 3
	{
		debugStart = GetPixelCoordinates (eventData);
		if (debugStart.x != -1) 
		{
			Debug.Log ("Local Image Coords (Pixel): " + debugStart);
		}

	}


	/// <summary>
	/// Raises the pointer up event. Prints the mouse location in pixels to the Debug Log
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerUp(PointerEventData eventData) // 3
	{
		debugEnd = GetPixelCoordinates (eventData);
		if (debugEnd.x != -1) 
		{
			float width = debugEnd.x - debugStart.x;
			float height = -1 * (debugEnd.y - debugStart.y);
			//Debug.Log ("Width: " + width + " Height: " + height);
		}

	}

	/// <summary>
	/// Takes the mouse click event data and returns a Vector2D with the corresponding location in a sprites rectTransform.
	/// </summary>
	/// <returns>Corresponfing image pixel coordinates.</returns>
	/// <param name="eventData">Event data.</param>
	Vector2 GetPixelCoordinates (PointerEventData eventData)
	{
		RectTransform imgRectTransform = GetComponent<RectTransform> ();
		Vector2 imgPixelCoords;


		//Get pixel coords of mouse input in image
		if (!RectTransformUtility.ScreenPointToLocalPointInRectangle (GetComponent<RectTransform> (), eventData.position, eventData.pressEventCamera, out imgPixelCoords)) 
		{
			Debug.Log ("Exiting function cause something went wrong");
			imgPixelCoords.x = -1;
			imgPixelCoords.y = -1;
			return imgPixelCoords;
		}
		//Debug.Log ("Image Width: " + imgRectTransform.rect.width);
		//Debug.Log("Before adjustment: " + imgPixelCoords);

		//factor for centre being 0.5/0.5.
		if (imgPixelCoords.x < 0) 
		{
			imgPixelCoords.x = imgPixelCoords.x + (int)imgRectTransform.rect.width / 2;
		}
		else 
		{
			imgPixelCoords.x += (int)imgRectTransform.rect.width / 2;
		}
		if (imgPixelCoords.y > 0) 
		{
			imgPixelCoords.y = imgPixelCoords.y + (int)imgRectTransform.rect.height / 2;
		}
		else 
		{
			imgPixelCoords.y += (int)imgRectTransform.rect.height / 2;
		}


		//Debug.Log("After Adjustment" + imgPixelCoords);
		return imgPixelCoords;
	} */

	/*
	/// <summary>
	/// Draws the waveform, given an array of samples.
	/// </summary>
	/// <param name="samples">Samples.</param>
	void DrawWaveform (float[] samples)
	{
		float prevX, prevY, currentX, currentY;
		prevX = -2;
		prevY = samples [0] * yAxisModifier;
		for (int i = sampleIncrement; i < samples.Length; i += sampleIncrement) {
			currentX = prevX + lineWidth;
			currentY = samples [i] * yAxisModifier;
			Debug.DrawLine (new Vector3 (prevX, prevY), new Vector3 (currentX, currentY), Color.green, lineDuration, false);
			Debug.Log ("line Drawn");
			prevX = currentX;
			prevY = currentY;
		}
	}
	*/
}
