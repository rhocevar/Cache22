﻿using UnityEngine;
using System.Collections;
[AddComponentMenu("Camera-Control/Mouse drag Orbit with zoom")]
public class CameraManager : Singleton<CameraManager>
{
	public Transform target;
	public float distance = 5.0f;
	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;
	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;
	public float distanceMin = .5f;
	public float distanceMax = 15f;
	public float smoothTime = 2f;
	float rotationYAxis = 0.0f;
	float rotationXAxis = 0.0f;
	float velocityX = 0.0f;
	float velocityY = 0.0f;
	//Animation
	bool animating = false;
	Transform animationTarget;
	CameraPosInfo camPosInfo;
	private float animTime;
	[SerializeField]
	private float animSpeed;
	// Use this for initialization
	void Start()
	{
		Vector3 angles = transform.eulerAngles;
		rotationYAxis = angles.y;
		rotationXAxis = angles.x;
		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
		{
			GetComponent<Rigidbody>().freezeRotation = true;
		}
		animTime = 0f;
	}
	void LateUpdate()
	{
		if (target && !FileBrowserManager.Instance.IsFilePanelActive())
		{
			if(!animating)
			{
				CursorOrbit ();
			}
			else //Animate camera to the correspondent cortex position
			{
				if(animationTarget)
				{
					AnimateTowardsTransform();
				}
			}
		}
	}

	private void AnimateTowardsTransform()
	{
		if(animTime <= 1.0f)
		{
			//Ease each value of the transform from the current to the target
			float posX = EasingFunction.Linear (transform.position.x, animationTarget.localPosition.x, animTime);
			float posY = EasingFunction.Linear (transform.position.y, animationTarget.localPosition.y, animTime);
			float posZ = EasingFunction.Linear (transform.position.z, animationTarget.localPosition.z, animTime);
			Vector3 newPosition = new Vector3(posX, posY, posZ);
			transform.position = newPosition;

			//Debug.Log ("AnimTime = " + animTime + "  posX = " + posX + " posY = " + posY + " posZ = " + posZ);

			Vector3 t180 = EulerAnglesTo180 (transform.rotation.eulerAngles);
			Vector3 target180 = EulerAnglesTo180 (animationTarget.rotation.eulerAngles);

			float rotX = EasingFunction.EaseOutCubic (t180.x, target180.x, animTime);
			float rotY = EasingFunction.EaseOutCubic (t180.y, target180.y, animTime);
			float rotZ = EasingFunction.EaseOutCubic (t180.z, target180.z, animTime);
			Vector3 newRotation = new Vector3(rotX, rotY, rotZ);
			transform.rotation = Quaternion.Euler(newRotation);

			//Debug.Log ("AnimTime = " + animTime + "  RotX = " + rotX + " RotY = " + rotY + " RotZ = " + rotZ);

			animTime += animSpeed * Time.deltaTime;
			if ((Mathf.Round(t180.x * 100f) / 100f) == (Mathf.Round(target180.x * 100f) / 100f) && 
				(Mathf.Round(t180.y * 100f) / 100f) == (Mathf.Round(target180.y * 100f) / 100f))
			{
				animTime = 1.1f;
			}
		}
		else
		{
			animating = false;
			animTime = 0.0f;
			animationTarget = null;
			//Debug.Log ("Animation finished.");
		}
	}

	private Vector3 EulerAnglesTo180(Vector3 ang)
	{
		Vector3 v = new Vector3 (ang.x, ang.y, ang.z);

		if(v.x > 180) v.x -= 360;
		if(v.y > 180) v.y -= 360;
		if(v.z > 180) v.z -= 360;

		return v;
	}

	public void SetAnimationTarget(Transform t)
	{
		animationTarget = t;
		animating = true;

		camPosInfo = animationTarget.GetComponent<CameraPosInfo> ();
		velocityX = camPosInfo.velocityX;
		velocityY = camPosInfo.velocityY;

		CalculateCamRotationAndPosition ();

		//Debug.Log ("Animation started...");
	}

	private void CursorOrbit()
	{
		if (Input.GetMouseButton(0) && Camera.main.pixelRect.Contains(Input.mousePosition))
		{
			velocityX += xSpeed * Input.GetAxis("Mouse X") * distance * 0.02f;
			velocityY += ySpeed * Input.GetAxis("Mouse Y") * 0.02f;
		}

		CalculateCamRotationAndPosition ();

	}

	private void CalculateCamRotationAndPosition()
	{
		rotationYAxis += velocityX;
		rotationXAxis -= velocityY;
		rotationXAxis = ClampAngle(rotationXAxis, yMinLimit, yMaxLimit);
		//Quaternion fromRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
		Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
		Quaternion rotation = toRotation;

		distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);
		RaycastHit hit;
		if (Physics.Linecast(target.position, transform.position, out hit))
		{
			distance = Mathf.Clamp(distance -= hit.distance, distanceMin, distanceMax);
		}

		Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
		Vector3 position = rotation * negDistance + target.position;

		//Debug.Log ("Rotation X: " + rotationXAxis + "Rotation Y: " + rotationYAxis);
		//Debug.Log ("Position: " + position + "Rotation: " + rotation);
		//Debug.Log ("VelocityX:  " + velocityX + "  VelocityY:  " + velocityY);

		if((velocityX < -0.01f || velocityX > 0.01f) || (velocityY < -0.01f || velocityY > 0.01f))
		{
			transform.rotation = rotation;
			transform.position = position;

			velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
			velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
		}
	}

	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}
}