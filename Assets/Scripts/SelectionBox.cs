﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectionBox : MonoBehaviour, IPointerDownHandler
{
	Vector2 startPos,endPos;
	RectTransform containerTransform;
	Rect selectionRectangle;
	FileContainer fileContainer;

	//flags
	bool  boxDrawn;
	bool mouseDownInContainer;

	[SerializeField]
	Texture textureImg;

	[SerializeField]
	float minimumWidth;

	[SerializeField]
	float yOffSet;

	// Use this for initialization
	void Start () 
	{
		ResetSelectionBox ();
		boxDrawn = false;
		mouseDownInContainer = false;
		containerTransform = GetComponent<RectTransform> ();
		selectionRectangle = Rect.zero;

		fileContainer = this.GetComponent<FileContainer> ();
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if (Input.GetKey(KeyCode.Mouse0))
		{
			BrainFile activeFile = fileContainer.GetActiveBrainFile();

//			if(Input.GetKeyDown(KeyCode.Mouse0))
//			{
//				/* catch first mouse down, this is handled in OnPointerDown event handler 
//				to ensure only clicks in this container are considered */
//			}
//			else //we are now in drag mode
//			{
				if (mouseDownInContainer) // only update endPos if selection box creation happened inside parent container
				{
					if (activeFile.fileType == BrainFile.BrainFileType.Image) 
					{
						//Debug.Log ("Updating EndPos");
						endPos = Input.mousePosition;
					} 
					else if (activeFile.fileType == BrainFile.BrainFileType.Audio) 
					{
						endPos = new Vector2 (Input.mousePosition.x, Screen.height);
					}
				
				}
//			}

		}

		else 
		{
			// Player was drawing a box and has now released the mouse
			if (startPos != Vector2.zero && endPos != Vector2.zero) 
			{
				
				float width, height;

				// check if mouse was released outside the container
				if (endPos.y < (Screen.height - containerTransform.rect.height)) 
				{
					Debug.Log ("Y-axis out of bounds");
					endPos.y = (Screen.height - containerTransform.rect.height) + 1;
				}

				if (endPos.x < (Screen.width - containerTransform.rect.width)) 
				{
					Debug.Log ("X-axis out of bounds");
					endPos.x = (Screen.width - containerTransform.rect.width) + 1;
				}


				width = endPos.x - startPos.x;
				height = -1 * (endPos.y - startPos.y);

				// save rectangle if its greater than the minimum width
				if (Mathf.Abs(width) >= minimumWidth) 
				{
					selectionRectangle = new Rect (startPos.x, Screen.height - startPos.y, width, height);
					boxDrawn = true;
					startPos = Vector2.zero;
					endPos = Vector2.zero;
					//Debug.Log ("enabling boxDrawn");
				} 
				else 
				{
					boxDrawn = false;
				}

				//reset flag
				mouseDownInContainer = false;
				
			}
		}
		
	}

	/// <summary>
	/// Resets the selectionBox.
	/// </summary>
	public void ResetSelectionBox()
	{
		startPos = Vector2.zero;
		endPos = Vector2.zero;
		boxDrawn = false;
	}

	void OnEnable()
	{
		ResetSelectionBox ();
	}

	/// <summary>
	/// Raises the GU event.
	/// </summary>
	void OnGUI()
	{
		//Debug.Log ("mouseDownInContainer: " + mouseDownInContainer);
		if (boxDrawn) {
			//Debug.Log ("drawing boxDrawn");
			GUI.DrawTexture (selectionRectangle, textureImg);
		} 
		else if (startPos != Vector2.zero && endPos != Vector2.zero && mouseDownInContainer) 
		{
			//Debug.Log ("drawing without boxDrawn");
			Rect tempRect = new Rect(startPos.x, Screen.height - startPos.y,
					endPos.x - startPos.x, -1 * (endPos.y - startPos.y));
			GUI.DrawTexture (tempRect, textureImg);
		}
	}

	/// <summary>
	/// Raises the pointer down event. Starts tracking selection box
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerDown(PointerEventData eventData)
	{
		//Debug.Log ("On Pointer Down");
		//BrainFile activeFile = fileContainer.GetActiveBrainFile ();
		BrainFile activeFile = fileContainer.GetActiveBrainFile();

		mouseDownInContainer = true;

		//if previous box exists, reset it
		if(boxDrawn)
		{
			//boxDrawn = false;
			ResetSelectionBox ();
		}

		if (activeFile.fileType == BrainFile.BrainFileType.Image) 
		{
			startPos = Input.mousePosition;
		} 
		else if (activeFile.fileType == BrainFile.BrainFileType.Audio) 
		{
			float yPos = (Screen.height - containerTransform.rect.height) + 1;
			startPos = new Vector2 (Input.mousePosition.x, yPos);
		}

	}

	/// <summary>
	/// Gets the selection rectangle in screen coordinates.
	/// </summary>
	/// <returns>The selection rectangle if one exists, otherwise returns a zero rectangle</returns>
	public Rect GetSelectionRectangleInScreen()
	{
		if (boxDrawn) 
		{
			return selectionRectangle;
		} 
		else 
		{
			return Rect.zero;
		}
	}


	/// <summary>
	/// Convert selection box into a new Rect object with pixel dimensions. 
	/// </summary>
	/// <returns>The selection box in pixels, or Rect zero if there is no valid selection box</returns>
	public Rect GetSelectionBoxInPixels()
	{
		if (boxDrawn) 
		{
			Vector2 startRectCoords, endRectCoords;
			float width, height;

			startRectCoords = pixelLocation(new Vector2(selectionRectangle.x, selectionRectangle.y));
			endRectCoords = pixelLocation(new Vector2(selectionRectangle.x + selectionRectangle.width, selectionRectangle.y + selectionRectangle.height));

			width = endRectCoords.x - startRectCoords.x;
			height = (endRectCoords.y - startRectCoords.y);

			Rect selectionBox_Pixels = new Rect (startRectCoords.x, startRectCoords.y, width, height);
			//Debug.Log("Selection Box in Pixels: " + selectionBox_Pixels);

			return selectionBox_Pixels;
		}
		return Rect.zero;

	}


	/// <summary>
	/// Converts a point from screen coordinates to local panel pixel coordinates.
	/// </summary>
	/// <returns>The point in pixel coordinates.</returns>
	/// <param name="screenLocation">Screen location.</param>
	private Vector2 pixelLocation(Vector2 screenLocation)
	{
		Vector2 pixelCoords;
		RectTransform imgRectTransform = this.GetComponent<RectTransform> ();
		
		//Get corresponding pixel coords of screen location
		if (!RectTransformUtility.ScreenPointToLocalPointInRectangle (GetComponent<RectTransform> (), screenLocation, null, out pixelCoords)) 
		{
			Debug.Log ("Exiting function cause something went wrong");
			pixelCoords.x = -1;
			pixelCoords.y = -1;
			return pixelCoords;
		}

		pixelCoords.x += (int)imgRectTransform.rect.width / 2;
		pixelCoords.y = (pixelCoords.y * -1) - yOffSet;


		return pixelCoords;
	}

}
