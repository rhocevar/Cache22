﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorSelectableObject : MonoBehaviour {

	[SerializeField]
	private Renderer[] meshes;
	[SerializeField]
	private string directory;
	[SerializeField]
	private GameObject cortexInfo;
	[SerializeField]
	private Transform cameraPosition;

	private bool selected;

	public bool Selected{ get { return selected;} set {selected = value;} }
	public string Directory{ get { return directory;} set {directory = value;} }
	public Renderer[] Meshes{ get{return meshes;} }
	public Transform CameraPosition {get {return cameraPosition;}}

	private void Start()
	{
		selected = false;
		ShowCortexInfo (false);
	}

	private void OnMouseEnter()
	{
		if(!selected && !FileBrowserManager.Instance.IsFilePanelActive())
		{
			SetMeshesMaterial (BrainManager.Instance.highlightMaterial);
		}
	}

	private void OnMouseExit()
	{
		if(!selected && !FileBrowserManager.Instance.IsFilePanelActive())
		{
			SetMeshesMaterial (BrainManager.Instance.DefaultMaterial);
		}
	}

	private void OnMouseDown()
	{
		if(!FileBrowserManager.Instance.IsFilePanelActive())
		{
			BrainManager.Instance.SelectCortex(directory);
			//Open respective folder
			FileBrowserManager.Instance.OpenDirectory(directory);

			SoundManager.Instance.PlaySound (0);
		}
	}

	public void SetMeshesMaterial(Material m)
	{
		foreach (Renderer rend in meshes) {
			rend.material = m;
		}
	}
	public void SetMeshesMaterialAnimated(Material m, float interval, int interations)
	{
		bool switchMat = false;
		float threadInterval = interval;
		float waitTime = 0.0f;
		int animIterations = interations;

		for(int i=0; i<=animIterations; i++)
		{
			if(i == animIterations)
			{
				StartCoroutine (SwitchMaterial (m, switchMat, waitTime, true));
			}
			else
			{
				StartCoroutine (SwitchMaterial (m, switchMat, waitTime, false));
			}
			switchMat = !switchMat;
			waitTime += threadInterval;
		}
	}

	public IEnumerator SwitchMaterial(Material m, bool switchMat, float tInterval, bool isFinished)
	{
		yield return new WaitForSeconds(tInterval);

		if(isFinished)
		{
			BrainManager.Instance.DefaultMaterial = BrainManager.Instance.BrainMaterials [BrainManager.Instance.CurrentChapter - 1];
			yield return new WaitForSeconds(0);
		}
		if(switchMat)
		{
			SetMeshesDefaultMaterial ();
		}
		else
		{
			SetMeshesMaterial (m);
		}
	}

	public void SetMeshesDefaultMaterial()
	{
		foreach (Renderer rend in meshes) {
			rend.material = BrainManager.Instance.DefaultMaterial;
		}
	}
	public void SetCortexSelected()
	{
		BrainManager.Instance.ClearBrainMaterials ();

		selected = true;
		SetMeshesMaterial (BrainManager.Instance.selectedMaterial);
		ShowCortexInfo (true);
	}
	public void ShowCortexInfo(bool b)
	{
		cortexInfo.SetActive (b);
	}
}
