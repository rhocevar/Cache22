﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager> {

	[SerializeField]
	private AudioClip[] soundEffects;

	private AudioSource sfxAudioSource;

	protected override void Awake()
	{
		base.Awake ();
		sfxAudioSource = GetComponent<AudioSource> ();
	}

	public void PlaySound(int audioClipIndex)
	{
		sfxAudioSource.clip = soundEffects [audioClipIndex];
		sfxAudioSource.Play ();
	}

}
