﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Search : MonoBehaviour 
{
	[SerializeField]
	Terminal terminal;
	[SerializeField]
	SelectionBox selectionBoxManager;
	[SerializeField]
	TagManager tagManager;
	[SerializeField]
	FileBrowserManager fileBrowser;
	Dictionary<string, BrainFile> fileCollection;
	float fileContainerPanel_area;

	string[] foundFiles;

	[SerializeField]
	float minOverlap_Threshold, maxOverlap_PanelThreshold;

	// Use this for initialization
	void Start () 
	{
		//fileCollection = fileBrowser.BrainFiles;
	}




	// Update is called once per frame
	void Update () {
		
	}




	/// <summary>
	/// Search the brain file database for any tags that match file selection.
	/// </summary>
	public void SearchBrainFiles()
	{
		BrainFile databaseFile;
		fileCollection = fileBrowser.BrainFiles;
		terminal.AddText ("\nSearching database.");
		if (CompareTags ()) 
		{
			terminal.AddText ("<color=#ffffffff>Links found.</color>");
			SoundManager.Instance.PlaySound (3);
			/*foreach (string fileName in foundFiles) 
			{
				terminalText += fileName + "\n";
			}
			terminal.AddText (terminalText);*/
			for (int i = 0; i < foundFiles.Length; i++) 
			{
				if (fileCollection.TryGetValue (foundFiles [i], out databaseFile)) 
				{
					RevealFile (databaseFile);
				}
				else 
				{
					Debug.Log ("Incorrect file name. Dictionary did not return a file");
				}

			} 

		} 
		else 
		{
			terminal.AddText ("<color=#ffffffff>Links not found</color>");
			SoundManager.Instance.PlaySound (2);
		}
		
	}

	/// <summary>
	/// Sets the isVisible property of a brain file so that it is now visible in the fileBrowser. Prints relevant text to the terminal.
	/// </summary>
	/// <param name="databaseFile">Database file.</param>
	void RevealFile (BrainFile databaseFile)
	{
		terminal.AddText("\t<color=#ffffffff>" + databaseFile.fileInfo.Name + "</color> found!");
		if (databaseFile.isVisible) 
		{
			terminal.AddText("\tAlready exists in Database.");
		}
		else 
		{
			databaseFile.isVisible = true;
			terminal.AddText("\tAdded to Databased.");
			BrainManager.Instance.CheckNewChapter (databaseFile.Chapter);
		}
	}


	/// <summary>
	/// Compares the current selection with the existing tags assigned to the file.
	/// </summary>
	/// <returns><c>true</c>, if selection overlaps with an existing tag, <c>false</c> otherwise.</returns>
	public bool CompareTags()
	{
		Rect selectionBox = selectionBoxManager.GetSelectionBoxInPixels ();
		Tag[] tagsInFile;
		string fileName;
		float intersect_area, tag_area, intersect_percentage, panelIntersect_percentage;
		float selectionArea;
		tagManager = FindObjectOfType<TagManager> ();
		//if no selection exists return 0
		if (selectionBox.x == 0) 
		{
			Debug.Log ("No valid selection box");
			return false;
		}

		//Calculate overlap % with UI panel
		Rect fileContainerRect = selectionBoxManager.gameObject.GetComponent<RectTransform> ().rect;
		fileContainerPanel_area = Mathf.Abs (fileContainerRect.width * fileContainerRect.height);

		selectionArea = Mathf.Abs(selectionBox.width*selectionBox.height);
		panelIntersect_percentage = (selectionArea / fileContainerPanel_area) * 100;
		//Debug.Log ("Overlap (Panel): " + panelIntersect_percentage + "%");

		//if user hasnt tried to cheat by selecting the whole panel (maxOverlap_PanelThreshold) then search for tags
		if (panelIntersect_percentage <= maxOverlap_PanelThreshold) 
		{
			fileName = fileBrowser.GetActiveFileName ();

			if (fileName == "" || fileName == null) 
			{
				Debug.Log ("No file loaded");
			} 
			else if (tagManager.Tags.TryGetValue (fileBrowser.GetActiveFileName (), out tagsInFile)) 
			{
				Rect tagRectangle;
				//foreach (Rect tag in tagsInFile)
				for(int i=0; i<tagsInFile.Length; i++)
				{
					tagRectangle = tagsInFile [i].GetActiveZone ();
					if (selectionBox.Overlaps (tagRectangle, true)) 
					{
						//Calculate overlap % with tag
						tag_area = Mathf.Abs (tagRectangle.width * tagRectangle.height);
						intersect_area = CalculateIntersectArea (tagRectangle, selectionBox);
						intersect_percentage = (intersect_area / tag_area) * 100;
						//Debug.Log ("Overlap (tag): " + intersect_percentage + "%");



						if (intersect_percentage >= minOverlap_Threshold) 
						{
							foundFiles = tagsInFile [i].GetConnectedFileNames();
							return true;
						} 
						return false;
					}
				}
			} 
		} 
		else 
		{
			terminal.AddText ("Data overload. Select a smaller area.");
		}

		return false;
	}


	/// <summary>
	/// Calculates the intersect area between a given tag and selection box.
	/// </summary>
	/// <returns>The intersect area.</returns>
	/// <param name="tag">Tag.</param>
	/// <param name="selectionBox">Selection box.</param>
	float CalculateIntersectArea(Rect tag, Rect selectionBox)
	{
		float intersect_width = 0, intersect_height = 0, intersect_area = 0;
		float tag_left, tag_right, tag_bottom, tag_top;
		float selectionBox_left, selectionBox_right, selectionBox_bottom, selectionBox_top;

		StandardiseRectangle (tag, out tag_left, out tag_right, out tag_bottom, out tag_top);
		StandardiseRectangle (selectionBox, out selectionBox_left, out selectionBox_right, out selectionBox_bottom, out selectionBox_top);

		//calculate intersection width

		//if selection box is wider than tag
		if (Mathf.Abs (selectionBox.width) >= Mathf.Abs (tag.width)) 
		{
			intersect_width = CalculateIntersectionWidth (tag, tag_left, tag_right, selectionBox_left, selectionBox_right);
		} 
		else 
		{
			intersect_width = CalculateIntersectionWidth (selectionBox, selectionBox_left, selectionBox_right, tag_left, tag_right);
		}


		//calculate intersection height


		if (Mathf.Abs (selectionBox.height) >= Mathf.Abs (tag.height)) 
		{
			intersect_height = CalculateIntersectionHeight (tag, tag_bottom, tag_top, selectionBox_bottom, selectionBox_top);
		} 
		else 
		{
			intersect_height = CalculateIntersectionHeight (selectionBox, selectionBox_bottom, selectionBox_top, tag_bottom, tag_top);
		}

		//Debug.Log ("Intersection Width: " + intersect_width + " Height: " + intersect_height);

		intersect_area = intersect_height * intersect_width;
		return intersect_area;
	}




	/// <summary>
	/// Calculates the height of the intersection on the assumption that rect B cannot be completely within rectA (vertically)
	/// </summary>
	/// <returns>The intersection height.</returns>
	/// <param name="RectA">Rect a.</param>
	/// <param name="rectA_bottom">Rect a bottom.</param>
	/// <param name="rectA_top">Rect a top.</param>
	/// <param name="rectB_bottom">Rect b bottom.</param>
	/// <param name="rectB_top">Rect b top.</param>
	private float CalculateIntersectionHeight (Rect RectA,  float rectA_bottom, float rectA_top, float rectB_bottom, float rectB_top)
	{
		float intersect_height;
		//top edge of tag is in the intesection
		if (rectA_top <= rectB_top && rectA_top >= rectB_bottom) 
		{
			// if the bottom edge of tag is also in the intersection
			if (rectA_bottom >= rectB_bottom) 
			{
				intersect_height = Mathf.Abs (RectA.height);
			}
			else 
			{
				intersect_height = Mathf.Abs (rectA_top - rectB_bottom);
			}
		}
		// otherwise the top edge of tag is outside the intersection. Is the botom edge of tag in the intersection
		else
		{
			intersect_height = Mathf.Abs (rectA_bottom - rectB_top);
		}
		return intersect_height;
	}



	/// <summary>
	/// Calculates the height of the intersection on the assumption that rect B cannot be completely within rectA (vertically)
	/// </summary>
	/// <returns>The intersection width.</returns>
	/// <param name="rectA">Rect a.</param>
	/// <param name="rectA_left">Rect a left.</param>
	/// <param name="rectA_right">Rect a right.</param>
	/// <param name="rectB_left">Rect b left.</param>
	/// <param name="rectB_right">Rect b right.</param>
	private float CalculateIntersectionWidth (Rect rectA, float rectA_left, float rectA_right, float rectB_left, float rectB_right)
	{
		float intersect_width;


		// if the left edge of rectA is in the intersection
		if (rectA_left >= rectB_left && rectA_left <= rectB_right) {
			// if the right edge of rectA is in the intersection, then the whole width of rectA is in the interection
			if (rectA_right <= rectB_right) 
			{
				intersect_width = Mathf.Abs (rectA.width);
			}
			else 
			{
				intersect_width = Mathf.Abs (rectB_right - rectA_left);
			}
		}

		//otherwise rectA left edge outside the intersection and the rectA right edge is in the intersection
		else 
		{
			intersect_width = Mathf.Abs (rectA_right - rectB_left);
		}
			
		return intersect_width;
	}




	/// <summary>
	/// use the old system of left, right, top, bottom instead of xmin, xmax, ymin, ymax to aid in intersection calculation. 
	/// This due to the fact that the rectangles may have negative widths and heights
	/// </summary>
	/// <param name="rectangle">Rectangle.</param>
	/// <param name="rect_left">Rect left.</param>
	/// <param name="rect_right">Rect right.</param>
	/// <param name="rect_bottom">Rect bottom.</param>
	/// <param name="rect_top">Rect top.</param>
	static void StandardiseRectangle (Rect rectangle, out float rect_left, out float rect_right, out float rect_bottom, out float rect_top)
	{
		if (rectangle.width < 0) 
		{
			rect_left = rectangle.xMax;
			rect_right = rectangle.xMin;
		}
		else 
		{
			rect_left = rectangle.xMin;
			rect_right = rectangle.xMax;
		}
		if (rectangle.height < 0) 
		{
			rect_top = rectangle.yMin;
			rect_bottom = rectangle.yMax;
		}
		else 
		{
			rect_top = rectangle.yMax;
			rect_bottom = rectangle.yMin;
		}
	}
}
