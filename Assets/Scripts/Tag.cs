﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Tag
{
	[SerializeField]
	string name;
	[SerializeField]
	Rect activeZone;
	[SerializeField]
	string[] connectedFileNames;


	public Rect GetActiveZone()
	{
		return activeZone;
	}

	public string[] GetConnectedFileNames()
	{
		return connectedFileNames;
	}

	public string GetName()
	{
		return name;
	}
	/// <summary>
	/// Search if a given filename is one of the files that this tag leads to.
	/// </summary>
	/// <returns><c>true</c>, if to file was connecteded, <c>false</c> otherwise.</returns>
	/// <param name="name">Name.</param>
	public bool ConnectedToFile(string name)
	{
		bool fileFound = false;

		foreach (string file in connectedFileNames) 
		{
			if (file.Equals(name, System.StringComparison.Ordinal) )
			{
				fileFound = true;
				break;
			}
		}

		return fileFound;
	}
}
