﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class CursorManager : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
	[SerializeField]
	private Texture2D cursorTexture;
	[SerializeField]
	private float offsetX, offsetY;
	[SerializeField]
	private Text[] coordinateTexts;

	private RectTransform horizontalLine;
	private RectTransform verticalLine;
	private CursorMode cursorMode = CursorMode.ForceSoftware;
	private Vector2 hotSpot;
	private bool isInsideRegion;
	private Vector3 tempVecHorizontal, tempVecVertical;
	private LayerMask brainMask;

	private void Awake()
	{
		horizontalLine = transform.FindChild ("HorizontalLine").GetComponent<RectTransform> ();
		verticalLine = transform.FindChild ("VerticalLine").GetComponent<RectTransform> ();
		brainMask = LayerMask.NameToLayer ("Brain");
	}

	private void Start()
	{
		tempVecHorizontal = new Vector3 (); tempVecVertical = new Vector3 ();
		hotSpot = new Vector2 (cursorTexture.width*0.5f, cursorTexture.height*0.5f);
		isInsideRegion = false;
		EnableLines (false);
		UpdateCoordinateTexts ();
	}

	public void OnPointerEnter( PointerEventData eventData ) {
		if(FileBrowserManager.Instance.IsFilePanelActive())
		{
			Cursor.SetCursor(null, Vector2.zero, cursorMode);
			isInsideRegion = false;
		}
		else
		{
			Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
			isInsideRegion = true;
		}
	}

	public void OnPointerExit( PointerEventData eventData ) {
		Cursor.SetCursor(null, Vector2.zero, cursorMode);
		isInsideRegion = false;
	}

	private void Update()
	{
		if(isInsideRegion)
		{
			EnableLines (true);
			UpdateCursorLines ();
			UpdateCoordinateTexts ();
		}
		else
		{
			EnableLines (false);
		}
	}

	private void UpdateCoordinateTexts()
	{
		Vector3 mousePos = Input.mousePosition;

		coordinateTexts [0].text = mousePos.x.ToString();
		coordinateTexts [1].text = mousePos.y.ToString();
		coordinateTexts [2].text = (mousePos.x*0.13).ToString();
		coordinateTexts [3].text = (mousePos.y*7).ToString();

//		RaycastHit hit;
//		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//		if (Physics.Raycast(ray, out hit))
//		{
//			if (hit.transform.gameObject.layer == brainMask) {
//				coordinateTexts [2].text = hit.point.z.ToString ();
//			}
//		}
	}

	private void EnableLines(bool b)
	{
		horizontalLine.gameObject.SetActive (b);
		verticalLine.gameObject.SetActive (b);
	}

	private void UpdateCursorLines()
	{
		tempVecVertical.x = Input.mousePosition.x - (hotSpot.x * 0.5f) + offsetX;
		tempVecVertical.y = verticalLine.position.y;
		tempVecVertical.z = verticalLine.position.z;
		verticalLine.position = tempVecVertical;

		tempVecHorizontal.x = horizontalLine.position.x;
		tempVecHorizontal.y = Input.mousePosition.y + (hotSpot.y * 0.5f) + offsetY;
		tempVecHorizontal.z = horizontalLine.position.z;
		horizontalLine.position = tempVecHorizontal;
	}

}