﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BrainManager : Singleton<BrainManager> {

	[SerializeField]
	private GameObject[] cortices;
	[SerializeField]
	private int maxChapters = 5;
	[SerializeField]
	Terminal terminal;
	[SerializeField]
	GameObject filePanel;

	[SerializeField]
	public Material highlightMaterial;
	[SerializeField]
	public Material selectedMaterial;
	[SerializeField]
	private Material[] brainMaterials;

	private Material defaultMaterial;
	public Material DefaultMaterial{ get{return defaultMaterial;} set { defaultMaterial = value; } }

	public Material[] BrainMaterials { get { return brainMaterials;} }

	private int currentChapter;
	public int CurrentChapter { get { return currentChapter;} set { currentChapter = value;} }

	private float currentDamage;
	private float damageAmountPerChapter;
	private float grayMinLimit = 0.7f;

	private void Start()
	{
		currentDamage = 0.0f;
		currentChapter = 1;
		damageAmountPerChapter = 100.0f / (maxChapters-1);
		defaultMaterial = brainMaterials [0];
	}

	public void CheckNewChapter(int c)
	{
		if(c > currentChapter)
		{
			currentChapter = c;
			currentDamage = (currentChapter-1) * damageAmountPerChapter;

			//Disable file panel to show brain
			for( int i = 0; i < filePanel.transform.childCount; i++ )
			{
				filePanel.transform.GetChild(i).gameObject.SetActive(false);
			}

			DamageBrain();
		}
	}

	private void DamageBrain()
	{
		//Set brain material
		SetBrainMaterial(brainMaterials[currentChapter-1], true);

		//Sound effect
		if(currentChapter == 5) // If final chapter
		{
			SoundManager.Instance.PlaySound(5);
			//Show brain damage % on terminal
			terminal.AddText("WARNING! BRAIN FULLY DAMAGED!");
			terminal.AddText("Damage percentage: " + currentDamage + "%");
		}
		else
		{
			SoundManager.Instance.PlaySound(4);
			//Show brain damage % on terminal
			terminal.AddText("WARNING! BRAIN DAMAGED!");
			terminal.AddText("Damage percentage: " + currentDamage + "%");
		}

	}

	private void SetBrainMaterial(Material m, bool anim)
	{
		foreach (GameObject o in cortices) {
			CursorSelectableObject cso = o.GetComponent<CursorSelectableObject> ();
			if(anim)
			{
				if(currentChapter == 5)
				{
					cso.SetMeshesMaterialAnimated (m, 0.07f, 100);
				}
				else
				{
					cso.SetMeshesMaterialAnimated (m, 0.075f, 20);
				}
			}
			else
			{
				cso.SetMeshesMaterial (m);
			}
		}
	}

	public void ClearBrainMaterials()
	{
		foreach (GameObject o in cortices) {
			CursorSelectableObject cso = o.GetComponent<CursorSelectableObject> ();
			cso.SetMeshesDefaultMaterial ();
			cso.Selected = false;
			cso.ShowCortexInfo (false);
		}
	}

	public void SelectCortex(string directoryName)
	{
		foreach (GameObject o in cortices) {
			CursorSelectableObject cso = o.GetComponent<CursorSelectableObject> ();
			if(cso.Directory == directoryName)
			{
				cso.SetCortexSelected ();
				CameraManager.Instance.SetAnimationTarget (cso.CameraPosition);
			}
		}
	}

}
