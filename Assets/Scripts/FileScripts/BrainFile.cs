﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class BrainFile : MonoBehaviour {

	public enum BrainFileType {Audio, Image, Video};  

	protected string resourcesPath = "Resources/";
	protected Texture2D fileTexture;
	protected bool isNew;
	protected int chapter;

	public FileInfo fileInfo { get; set; }
	public BrainFileType fileType { get; set; }
	public Tag[] tags;
	public bool isVisible { get; set; }
	public Texture2D FileTexture{ get{return fileTexture;} set{fileTexture = value;} }
	public int Chapter { get { return chapter; } }

	public BrainFile() {
		isVisible = false;
		isNew = true;
		fileTexture = FileBrowserManager.Instance.DefaultFileTexture;
	}

	public virtual void SetIsNew(bool b) { 
		isNew = b;
	}

	public bool GetIsNew()
	{
		return isNew;
	}

	protected void SetFileVisibility()
	{
		if(TagManager.Instance.VisibleFiles.Contains(fileInfo.Name))
		{
			isVisible = true;
		}
		else
		{
			isVisible = false;
		}
	}

	protected void SetFileChapterByFilename()
	{
		string filename = fileInfo.Name;

		//Get the chapter int value based on the first character of the filename
		int ch = (int)Char.GetNumericValue(filename [0]);
		if(ch >= 0)
		{
			chapter = ch;
			//Debug.Log ("Chapter set " + chapter + " for file " + filename);
		}
		else
		{
			//Debug.Log ("Could not find chapter for file: " + filename);
		}
	}
}
