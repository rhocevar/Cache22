﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AudioFile : BrainFile {

	private Texture2D image;
	public Texture2D Image{ get {return image;} set{this.image = value;} }

	private AudioClip clip;
	public AudioClip Clip{ get { return clip; } set { this.clip = value; } }

	public AudioFile(FileInfo f)
	{
		fileInfo = f;
		fileType = BrainFileType.Audio;
		image = ImageFromFile (fileInfo);
		clip = AudioFromFile (fileInfo);
		SetFileVisibility ();
		fileTexture = FileBrowserManager.Instance.FileTextures [0]; //0 - New audio texture
		SetFileChapterByFilename ();
	}

	public override void SetIsNew (bool b)
	{
		base.SetIsNew (b);
		if(b == true)
		{
			fileTexture = FileBrowserManager.Instance.FileTextures [0]; //0 - New audio texture
		}
		else
		{
			fileTexture = FileBrowserManager.Instance.FileTextures [1]; //1 - Regular audio texture
		}
	}

	private Texture2D ImageFromFile(FileInfo f)
	{
		Texture2D imageFile = new Texture2D(2, 2);

		if(!imageFile.LoadImage (File.ReadAllBytes (f.FullName)))
		{
			Debug.Log ("Problem reading image file: " + f.Name);
		}
		return imageFile;
	}

	private AudioClip AudioFromFile(FileInfo f)
	{
		int resIndex = fileInfo.FullName.LastIndexOf (resourcesPath) + resourcesPath.Length;
		string filePath = fileInfo.FullName.Substring (resIndex);

		filePath = filePath.Substring(0, filePath.Length-4); //Remove the extension
		return (AudioClip) Resources.Load(filePath + "_audio");
	}

}
