﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ImageFile : BrainFile {

	private Texture2D image;

	public Texture2D Image{ get {return image;} set{this.image = value;} }

	public ImageFile(FileInfo f)
	{
		fileInfo = f;
		fileType = BrainFileType.Image;
		image = ImageFromFile (fileInfo);
		SetFileVisibility ();
		fileTexture = FileBrowserManager.Instance.FileTextures [2]; //2 - New image texture
		SetFileChapterByFilename ();
	}

	public override void SetIsNew (bool b)
	{
		base.SetIsNew (b);
		if(b == true)
		{
			fileTexture = FileBrowserManager.Instance.FileTextures [2]; //2 - New image texture
		}
		else
		{
			fileTexture = FileBrowserManager.Instance.FileTextures [3]; //3 - Regular image texture
		}
	}

	private Texture2D ImageFromFile(FileInfo f)
	{
		int resIndex = fileInfo.FullName.LastIndexOf (resourcesPath) + resourcesPath.Length;
		string filePath = fileInfo.FullName.Substring (resIndex);

		filePath = filePath.Substring(0, filePath.Length-4); //Remove the extension
		return (Texture2D) Resources.Load(filePath);
	}
}
