﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class VideoFile : BrainFile {

	private MovieTexture video;
	public MovieTexture Video {get{return video;} set{video = value;}}

	public VideoFile(FileInfo f)
	{
		fileInfo = f;
		fileType = BrainFileType.Video;
		video = VideoFromFile (fileInfo);
		SetFileVisibility ();
		fileTexture = FileBrowserManager.Instance.FileTextures [4]; //4 - New video texture
		SetFileChapterByFilename ();
	}

	public override void SetIsNew (bool b)
	{
		base.SetIsNew (b);
		if(b == true)
		{
			fileTexture = FileBrowserManager.Instance.FileTextures [4]; //4 - New video texture
		}
		else
		{
			fileTexture = FileBrowserManager.Instance.FileTextures [5]; //5 - Regular video texture
		}
	}

	private MovieTexture VideoFromFile(FileInfo f)
	{
		int resIndex = fileInfo.FullName.LastIndexOf (resourcesPath) + resourcesPath.Length;
		string filePath = fileInfo.FullName.Substring (resIndex);

		filePath = filePath.Substring(0, filePath.Length-4); //Remove the extension
		return (MovieTexture) Resources.Load(filePath);
	}
}
