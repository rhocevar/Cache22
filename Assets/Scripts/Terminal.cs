﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Terminal : MonoBehaviour 
{
	Text terminalText;
	ScrollRect scrollBar;
	[SerializeField]
	float lineHeight;

	// Use this for initialization
	void Start () 
	{
		terminalText = GetComponent<Text> ();
		scrollBar = FindObjectOfType<ScrollRect> ();
	}
	
	// Update is called once per frame q
	void Update () 
	{
		
	}

	/// <summary>
	/// Append a new line of text in the terminal
	/// </summary>
	/// <param name="statusText">Status text.</param>
	public void AddText(string statusText)
	{
		terminalText.text += statusText +"\n";

		//scrollBar.verticalNormalizedPosition = 0;
		scrollBar.normalizedPosition = Vector2.zero;
	}

	/// <summary>
	/// Resets the terminal and ereases all existing text
	/// </summary>
	public void ResetTerminal()
	{
		terminalText.text = "";
	}
}