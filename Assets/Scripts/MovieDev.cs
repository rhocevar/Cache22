﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;
using System;

public class MovieDev:MonoBehaviour
{
	[SerializeField]
	private GameObject imageOverlay;

	private MovieTexture mt;
	private AudioSource audioSource;
	private RawImage rim;

	private float duration = 0.0f;
	private float endOfClipTreshold = 0.4f;

	private void Init()
	{
		rim = GetComponent<RawImage>();
		audioSource = GetComponent<AudioSource> ();
	}

	private void Update()
	{
		if (mt.isPlaying) {
			duration += Time.deltaTime;
			//Debug.Log ("Time = " + duration + " / " + mt.duration);
			if(duration >= mt.duration - endOfClipTreshold)
			{
				//If movie reached the end, rewind it
				//Debug.Log("End of movie.");
				Rewind();
			}
		}
	}

	private void Rewind()
	{
		mt.Stop ();
		audioSource.Stop ();
		imageOverlay.SetActive (true);
		duration = 0.0f;
	}


	public void SetMovieTexture(MovieTexture t)
	{
		if (!rim) Init ();
		rim.texture = t;
		mt = (MovieTexture)rim.mainTexture;
		audioSource.clip = mt.audioClip;
		duration = 0.0f;
	}

	public void OnVideoButtonClick()
	{
		if (mt.isPlaying)
		{
			mt.Pause();
			audioSource.Pause ();
			duration += Time.deltaTime * 2;
		}
		else
		{
			imageOverlay.SetActive (false);
			mt.Play();
			audioSource.UnPause ();
		}
	}

	public void OnBackButtonClick()
	{
		mt.Stop ();
		audioSource.Stop ();
		imageOverlay.SetActive (true);
		transform.parent.gameObject.SetActive (false);
	}
}