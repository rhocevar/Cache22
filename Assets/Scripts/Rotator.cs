﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour
{
	[SerializeField]
	private float speed;
	// Update is called once per frame
	void Update()
	{
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			transform.Rotate(new Vector3(0, 1, 0) * Time.deltaTime * speed);
		}
		else if(Input.GetKey(KeyCode.RightArrow))
		{
			transform.Rotate(new Vector3(0, -1, 0) * Time.deltaTime * speed);
		}

		if(Input.GetKey(KeyCode.UpArrow))
		{
			transform.Rotate(new Vector3(1, 0, 0) * Time.deltaTime * speed);
		}
		else if(Input.GetKey(KeyCode.DownArrow))
		{
			transform.Rotate(new Vector3(-1, 0, 0) * Time.deltaTime * speed);
		}
	}
}
