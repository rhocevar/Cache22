﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class FileTagsPair
{
	[SerializeField]
	public string filename;
	[SerializeField]
	public Tag[] tags;
}

public class TagManager : Singleton<TagManager> {

	[SerializeField]
	private List<string> visibleFiles;
	private Dictionary<string, Tag[]> tags;

	public Dictionary<string, Tag[]> Tags { get{return tags;} }

	public List<string> VisibleFiles { get {return visibleFiles;} }

	[SerializeField]
	private FileTagsPair[] fileTags;

	private void Start()
	{
		tags = new Dictionary<string, Tag[]> ();

		foreach(FileTagsPair ftp in fileTags)
		{
			tags.Add (ftp.filename, ftp.tags);
		}
	}
}
